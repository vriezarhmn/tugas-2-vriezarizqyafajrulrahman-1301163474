# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer

# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler
import threading

# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

# Buat server
server = SimpleXMLRPCServer(('localhost',8000),requestHandler = RequestHandler))

    # buat data struktur dictionary untuk menampung nama_kandidat dan hasil voting
    candidate = {
        'DragonBall' : 0 ,
        'SlamDunk' : 0 ,
        'Gundam' : 0
    }
    
    # kode setelah ini adalah critical section, menambahkan vote tidak boeh terjadi race condition
    # siapkan lock
    lock = threading.Lock()
    
    #  buat fungsi bernama vote_candidate()
    def vote_candidate(x):
        
        # critical section dimulai harus dilock
        lock.acquire()
        # jika kandidat ada dalam dictionary maka tambahkan  nilai votenya
        if x in candidate:
            candidate[x] += 1
        
        # critical section berakhir, harus diunlock
        lock.release()
        return True
        
    
    # register fungsi vote_candidate() sebagai vote
    server.register_function(vote_candidate,'vote')

    # buat fungsi bernama querry_result
    def querry_result():
        # critical section dimulai
        lock.acquire()
        
        # hitung total vote yang ada
        total = sum(candidate.values())
        
        # hitung hasil persentase masing-masing kandidat
        for x in candidate:
            print(x,": ",cabdudate[x]/total*100,"%")
        
        # critical section berakhir
        lock.release()    
        
    # register querry_result sebagai querry
    server.register_function(querry_result,"querry")


    print ("Server voting berjalan...")
    # Jalankan server
    server.serve_forever()
