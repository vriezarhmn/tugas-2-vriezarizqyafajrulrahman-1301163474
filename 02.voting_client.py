# import xmlrpc bagian client saja
import xmlrpc.client

# buat stub (proxy) untuk client
stub = xmlrpc.client.ServerProxy('http://localhost:8000', allow_none=True)

# lakukan pemanggilan fungsi vote("nama_kandidat") yang ada di server
stub.vote('DragonBall')

# lakukan pemanggilan fungsi querry() untuk mengetahui hasil persentase dari masing-masing kandidat
stub.querry()

# lakukan pemanggilan fungsi lain terserah Anda
stub.vote("SlamDunk")
stub.vote("SlamDunk")
stub.vote("Gundam")
stub.querry()